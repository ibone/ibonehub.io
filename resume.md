# 简介
- 85后
- 十年Web开发经验
- 魔兽世界资深玩家
- 漂移板爱好者
- 喜欢前端技术，多变它恰恰是它的魅力所在
- 对富文本编辑器有丰富的经验

# 联系方式
- 博客内容太老旧，被我关了
- ibonework@gmail.com
- [https://www.instagram.com/iboneyu/](https://www.instagram.com/iboneyu/)
- [https://twitter.com/ibonelu](https://twitter.com/ibonelu)

# 最近工作经历
### **前端Leader** - 广州带我飞科技有限公司, 2017
主要应用技术：Vue, Webpack
<table>
  <tbody>
    <tr>
      <td align="center" valign="middle" width="100">
        <img width="50" src="https://github.com/ibone/ibone.github.io/blob/master/assets/logo2.png?raw=true">
      </td>
      <td align="center" valign="middle" width="100">
      <a href="http://dwfei.com" title="带我飞">带我飞</a>
      </td>
      <td align="left" valign="middle">
      一款旅游类App，主打全球特价机票搜索和购买<br>
      </td>
    </tr>
  </tbody>
</table>

### **高级前端工程师** - 北京锤子科技有限公司, 2014
主要应用技术：Angular, Gulp
<table>
  <tbody>
    <tr>
      <td align="center" valign="middle" width="100">
        <img src="https://static.smartisanos.cn/cloud/index/img/sidebar/cloud_324e10b99d.png">
      </td>
      <td align="center" valign="middle" width="100">
      <a href="https://yun.smartisan.com/#/" title="欢喜云">欢喜云</a>
      </td>
      <td align="left" valign="middle">
      用户在PC端也可以使用锤子手机自研的App
      </td>
    </tr>
    <tr>
      <td align="center" valign="middle">
        <img src="https://static.smartisanos.cn/cloud/index/img/sidebar/note_d606693c40.png">
      </td>
      <td align="center" valign="middle"><a href="https://www.smartisan.com/apps/notes">锤子便签</a></td>
      <td align="left" valign="middle">支持图片插入，图文混排，还可以随时随地将便签内容生成精美的长微博或网页并分享</td>
    </tr>
    <tr>
      <td align="center" valign="middle">
        <img src="https://static.smartisanos.cn/cloud/index/img/sidebar/find_04bfb5319f.png">
      </td>
      <td align="center" valign="middle">手机雷达</td>
      <td align="left" valign="middle">用户手机丢失后，可定位查找，内置高德地图，Google地图</td>
    </tr>
  </tbody>
</table>

# 开源项目
- ubb-editor https://github.com/ibone/ubb-editor （guang.com的富文本编辑器，guang.com已停止运营，Google搜索图片还有些记录，哎）
- chameleon-mock https://github.com/ibone/chameleon-mock （e2e自动化测试数据mock服务）
- seven-words https://github.com/ibone/seven-words 开发中 (Mac端背词工具，基于Electron开发，主要是自用)
- responseMaker https://github.com/ibone/responseMaker （chameleon-mock进化版） 开发中

# 技术栈
常用技术: 
- Vue, Angular, Rails, Express
- Webpack, Babel, Eslint, Stylus, Fabric
- Jasmine, Mocha, Karma 

常用语言: 
- Javascript
- NodeJS
- Python
- Ruby

对 Flutter 很感兴趣，目前一有时间就写点 Demo，看好它的移动端跨平台解决方案

# 教育经历
- 福州大学 Web应用程序设计
